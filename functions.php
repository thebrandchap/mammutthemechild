<?php

add_action( 'woocommerce_email_before_order_table', 'add_consent_form_links', 10, 2 );
function add_consent_form_links( $order, $is_admin_email ) {
    echo '<h3>*IMPORTANT*</h3><p>If you are joining us on a course, everyone on the course must have filled in a <a href="https://mammutmountainschool.co.uk/consent-forms/">consent form</a> for their booking to be valid.</p><p><a href="https://mammutmountainschool.co.uk/consent-forms/">Download the appropriate PDF consent form</a>, fill it in, and email it back to us <b>at least 1 week prior to your course start date.</b></p>';
}
